FROM openjdk:11

ADD build/libs/hello-world-0.1.0.jar helloWorld.jar

ENTRYPOINT ["java","-jar","/helloWorld.jar"]